package classdia

import (
	"errors"
	"io"
	"strings"
)

// Parses a diagram from the passed in reader
func Parse(r io.Reader, filename string) (*Diagram, error) {
	astDiagram, err := parse(r, filename)
	if err != nil {
		return nil, err
	}

	treeBuilder := &treeBuilder{
		diagram: &Diagram{},
		types:   make(map[string]*Class),
	}

	err = treeBuilder.build(astDiagram)
	if err != nil {
		return nil, err
	}

	return treeBuilder.diagram, nil
}

// Constructs a diagram from an AST tree
type treeBuilder struct {
	diagram      *Diagram
	types        map[string]*Class
	pendingLinks []*tbPendingLink
}

func (tb *treeBuilder) build(node *astDiagram) error {
	// Build the base diagram items
	err := tb.buildDiagramItems(node.items)
	if err != nil {
		return err
	}

	// Link parents
	err = tb.connectParents()
	if err != nil {
		return err
	}

	// Connect the pending links
	err = tb.connectPendingLinks()
	if err != nil {
		return err
	}

	return nil
}

func (tb *treeBuilder) connectParents() error {
	// TODO
	return nil
}

func (tb *treeBuilder) connectPendingLinks() error {
	for _, pendingLink := range tb.pendingLinks {
		err := pendingLink.addToGraph(tb)
		if err != nil {
			return err
		}
	}

	return nil
}

func (tb *treeBuilder) buildDiagramItems(items *astDiagramItem) error {
	for ; items != nil; items = items.next {
		err := tb.buildDiagramItem(items.item)
		if err != nil {
			return err
		}
	}

	return nil
}

func (tb *treeBuilder) buildDiagramItem(item astDiagramItemType) error {
	switch t := item.(type) {
	case *astTypeDef:
		return tb.buildTypeDef(t)
	default:
		return errors.New("Internal error: unrecognised diagram item")
	}
}

func (tb *treeBuilder) buildTypeDef(typeDef *astTypeDef) error {
	cls := &Class{}

	stereotypeLabel := strings.TrimSpace(typeDef.stereotype)
	if stereotypeLabel == "" {
		cls.Stereotype = NoStereotype
	} else if stereotype, hasStereotype := stereotypeMap[stereotypeLabel]; hasStereotype {
		cls.Stereotype = stereotype
	} else {
		cls.Stereotype = OtherStereotype
	}

	cls.Name = typeDef.name
	cls.StereotypeLabel = typeDef.stereotype

	// TODO: Add parent

	for typeItem := typeDef.items; typeItem != nil; typeItem = typeItem.next {
		err := tb.buildTypeItem(cls, typeItem.item)
		if err != nil {
			return err
		}
	}

	tb.diagram.Classes = append(tb.diagram.Classes, cls)
	tb.types[typeDef.name] = cls

	return nil
}

func (tb *treeBuilder) buildTypeItem(cls *Class, typeItem astTypeItemType) error {
	switch t := typeItem.(type) {
	case *astAttribute:
		return tb.buildAttribute(cls, t)
	case *astMethod:
		return tb.buildMethod(cls, t)
	case *astLink:
		// Add the link to the pending links
		tb.pendingLinks = append(tb.pendingLinks, &tbPendingLink{t, cls})
		return nil
	default:
		return errors.New("Internal error: unrecognised type item")
	}
}

func (tb *treeBuilder) buildAttribute(cls *Class, attrNode *astAttribute) error {
	attr := &Attr{}

	visability, hasVisability := runeVisabilityMap[attrNode.visability]
	if !hasVisability {
		return errors.New(attrNode.pos.String() + ": invalid visability: " + string(attrNode.visability))
	}

	attr.Visability = visability
	attr.Name = attrNode.name
	attr.Type = TypeDef(attrNode.typeName)

	cls.Attrs = append(cls.Attrs, attr)
	return nil
}

func (tb *treeBuilder) buildMethod(cls *Class, attrNode *astMethod) error {
	var err error
	method := &Method{}

	visability, hasVisability := runeVisabilityMap[attrNode.visability]
	if !hasVisability {
		return errors.New(attrNode.pos.String() + ": invalid visability: " + string(attrNode.visability))
	}

	method.Visability = visability
	method.Name = attrNode.name
	if method.Args, err = tb.buildArgList(cls, method, attrNode.args); err != nil {
		return err
	}
	if method.Results, err = tb.buildArgList(cls, method, attrNode.rets); err != nil {
		return err
	}

	cls.Methods = append(cls.Methods, method)
	return nil
}

func (tb *treeBuilder) buildArgList(cls *Class, method *Method, attrNode *astArgItemList) ([]*Argument, error) {
	args := make([]*Argument, 0)
	for ; attrNode != nil; attrNode = attrNode.next {
		argItem := attrNode.item
		argument := &Argument{
			Name: argItem.name,
			Type: TypeDef(argItem.typeName),
		}
		args = append(args, argument)
	}
	return args, nil
}

// A link to construct.  These are done after the types have been created
// to allow for circular dependencies.
type tbPendingLink struct {
	ast       *astLink
	sourceCls *Class
}

func (link *tbPendingLink) addToGraph(tb *treeBuilder) error {
	targetType, hasTargetType := tb.types[link.ast.target]
	if !hasTargetType {
		return errors.New(link.ast.pos.String() + ": link target '" + link.ast.target + "' not defined.")
	}

	linkType, hasLinkType := linkTypeMap[link.ast.linkType]
	if !hasLinkType {
		return errors.New(link.ast.pos.String() + ": link type '" + link.ast.linkType + "' not defined.")
	}

	linkModel := &Link{
		From:      link.sourceCls,
		To:        targetType,
		Type:      linkType,
		Label:     link.ast.linkLabel,
		FromLabel: link.ast.fromLabel,
		ToLabel:   link.ast.toLabel,
	}

	tb.diagram.Links = append(tb.diagram.Links, linkModel)

	return nil
}

var runeVisabilityMap = map[rune]Visability{
	'\x00': NoVisability,
	'+':    PublicVisability,
	'-':    PrivateVisability,
	'#':    ProtectedVisability,
}

var stereotypeMap = map[string]Stereotype{
	"interface": InterfaceStereotype,
	"abstract":  AbstractStereotype,
	"external":  ExternalStereotype,
}

var linkTypeMap = map[string]LinkType{
	"aggregate_of": AggregationLink,
	"references":   ReferenceLink,
	"extends":      InheritanceLink,
	"implements":   RealizationLink,
}
