//go:generate go tool yacc -o parser.go parser.y
package classdia

import (
	"text/scanner"
)

type astDiagram struct {
	items *astDiagramItem
}

type astDiagramItem struct {
	item astDiagramItemType
	next *astDiagramItem
}

type astDiagramItemType interface{}

type astTypeDef struct {
	pos scanner.Position

	stereotype string
	name       string
	alias      string
	superTypes *astStringList
	items      *astTypeItem
}

type astTypeItem struct {
	item astTypeItemType
	next *astTypeItem
}

type astTypeItemType interface{}

type astAttribute struct {
	pos scanner.Position

	visability rune
	name       string
	typeName   string
}

type astMethod struct {
	pos scanner.Position

	visability rune
	name       string
	args       *astArgItemList
	rets       *astArgItemList
}

type astLink struct {
	pos scanner.Position

	linkType  string
	linkLabel string
	target    string
	fromLabel string
	toLabel   string
}

type astStringList struct {
	pos scanner.Position

	item string
	next *astStringList
}

type astArgItemList struct {
	item *astArgItem
	next *astArgItemList
}

type astArgItem struct {
	pos scanner.Position

	name     string
	typeName string
}
