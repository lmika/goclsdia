package classdia

import (
	"bytes"
	"io"
	//    "fmt"

	"github.com/awalterschulze/gographviz"
)

// Renderer of a class diagram which will render the class diagram
// to a .dot file
type DotRenderer struct {
	// Font to use
	Font string

	// Orientation of the graph
	Orientation GraphOrientation

	// When false, forces the use of 7 bit ASCII.  Otherwise, use UTF-8
	// characters like guillemets for stereotypes
	ForceAscii bool
}

// Renders the class diagram
func (dr *DotRenderer) RenderTo(diagram *Diagram, w io.Writer) error {
	di := &dotRenderInstance{
		graph:   gographviz.NewGraph(),
		diagram: diagram,
		font:    dr.Font,
		orientationAttributes: orientationAttributesMap[dr.Orientation],
		stereotypeCaps:        labelCaps[dr.ForceAscii],
		edgeLabelCaps:         labelCaps[dr.ForceAscii],
	}

	di.doRender()

	_, err := io.WriteString(w, di.graph.String())
	return err
}

// Maintains caps for labels
type LabelCaps struct {
	Start string
	End   string
}

// Maintains the context for a particular dot rendering session
type dotRenderInstance struct {
	graph                 *gographviz.Graph
	diagram               *Diagram
	font                  string
	orientationAttributes *orientationAttributes
	stereotypeCaps        LabelCaps
	edgeLabelCaps         LabelCaps
}

// Runs the renderer with the given diagram
func (di *dotRenderInstance) doRender() error {
	if err := di.initialize(); err != nil {
		return err
	}

	// Renders the classes
	for _, cls := range di.diagram.Classes {
		di.addClassToGraph(cls)
	}

	// Render the links
	for _, link := range di.diagram.Links {
		di.addLinkToGraph(link)
	}

	return nil
}

// Initialise the graphvis graph
func (di *dotRenderInstance) initialize() error {
	var err error
	di.graph.Name = "classdia"
	di.graph.Directed = true

	if di.graph.Attrs, err = gographviz.NewAttrs(map[string]string{
		"rankdir": di.orientationAttributes.dotRankDir, // Rank from top to bottom
		"splines": "false",
	}); err != nil {
		return err
	}

	return nil
}

// Adds a class definition to the graph
func (di *dotRenderInstance) addClassToGraph(cls *Class) {
	nodeName := di.classToNodeName(cls)
	nodeAttrs := map[string]string{
		"label":    di.classToNodeLabel(cls),
		"shape":    "record",
		"fontname": di.renderString(di.font),
	}

	if stereotypeNodeAttrs, hasStereotypeNodeAttrs := stereotypeNodeAttributes[cls.Stereotype]; hasStereotypeNodeAttrs {
		nodeAttrs = stereotypeNodeAttrs.addAttrs(nodeAttrs)
	}

	di.graph.AddNode("", nodeName, nodeAttrs)
}

// Adds a link definition to a graph
func (di *dotRenderInstance) addLinkToGraph(link *Link) {
	src := di.classToNodeName(link.From)
	dst := di.classToNodeName(link.To)

	attrs := edgeAttributes[link.Type].addAttrs(make(map[string]string))
	attrs["minlen"] = "1.75"
	attrs["fontname"] = di.renderString(di.font)

	if link.Label != "" {
		attrs["label"] = di.renderString(di.edgeLabelCaps.Start + link.Label + di.edgeLabelCaps.End)
	}
	if link.FromLabel != "" {
		attrs["headlabel"] = di.renderString(link.ToLabel)
		attrs["labeldistance"] = "1.8"
	}
	if link.ToLabel != "" {
		attrs["taillabel"] = di.renderString(link.FromLabel)
		attrs["labeldistance"] = "1.8"
	}

	di.graph.AddEdge(src, dst, true, attrs)
}

// Returns the node name for a class
func (di *dotRenderInstance) classToNodeName(cls *Class) string {
	return cls.Name
}

// Generates a node label for the class
func (di *dotRenderInstance) classToNodeLabel(cls *Class) string {
	labelBfr := &bytes.Buffer{}

	if cls.Stereotype != NoStereotype {
		labelBfr.WriteString(di.stereotypeCaps.Start + di.escapeRunesForLabel(cls.StereotypeLabel) + di.stereotypeCaps.End)
		labelBfr.WriteString("\\n")
	}
	labelBfr.WriteString(di.escapeRunesForLabel(cls.Name))

	// Write the attributes if any are defined
	if len(cls.Attrs) > 0 {
		labelBfr.WriteString(" | ")
		for i, attr := range cls.Attrs {
			if i > 0 {
				labelBfr.WriteString("\\n")
			}
			labelBfr.WriteString(di.escapeRunesForLabel(attr.String()))
		}
	}

	// Write the methods if any are defined
	if len(cls.Methods) > 0 {
		if len(cls.Attrs) == 0 {
			// Add a blank segment to indicate that there are methods.
			labelBfr.WriteString(" | ")
		}

		labelBfr.WriteString(" | ")
		for i, method := range cls.Methods {
			if i > 0 {
				labelBfr.WriteString("\\n")
			}
			labelBfr.WriteString(di.escapeRunesForLabel(method.String()))
		}
	}

	if di.orientationAttributes.nodesRequireBrackets {
		return "\"{ " + labelBfr.String() + " }\""
	} else {
		return "\" " + labelBfr.String() + " \""
	}
}

// Renders a label value from a string
func (di *dotRenderInstance) renderString(s string) string {
	return "\"" + s + "\""
}

// Quote special characters from a string buffer
func (di *dotRenderInstance) escapeRunesForLabel(s string) string {
	chars := make([]rune, 0, len(s))
	for _, r := range s {
		if di.shouldEscapeRuneForLabel(r) {
			chars = append(chars, '\\')
		}
		chars = append(chars, r)
	}

	return string(chars)
}

// Returns true if the given rune should be escaped in node labels
func (di *dotRenderInstance) shouldEscapeRuneForLabel(r rune) bool {
	return (r == '{') || (r == '}') || (r == '<') || (r == '>') || (r == '"') || (r == '|')
}

// Graph orientation
type GraphOrientation int

const (
	Vertical GraphOrientation = iota
	Horizontal
)

// Attributes based on the orientation
type orientationAttributes struct {
	dotRankDir           string
	nodesRequireBrackets bool
}

var orientationAttributesMap = map[GraphOrientation]*orientationAttributes{
	Vertical: {
		dotRankDir:           "BT",
		nodesRequireBrackets: true,
	},

	Horizontal: {
		dotRankDir:           "RL",
		nodesRequireBrackets: false,
	},
}

// Node attributes
type nodeAttribute struct {
	style string
}

func (na *nodeAttribute) addAttrs(attr map[string]string) map[string]string {
	setIfNotEmpty(attr, "style", na.style)
	return attr
}

// Default attributes for an edge
type edgeAttribute struct {
	dir         string
	arrowHead   string
	lineStyle   string
	noConstrain bool
}

func (ea *edgeAttribute) addAttrs(attr map[string]string) map[string]string {
	setIfNotEmpty(attr, "dir", ea.dir)
	setIfNotEmpty(attr, "arrowhead", ea.arrowHead)
	setIfNotEmpty(attr, "style", ea.lineStyle)

	if ea.noConstrain {
		setIfNotEmpty(attr, "constraint", "false")
	}

	return attr
}

func setIfNotEmpty(attr map[string]string, attrName, attrValue string) {
	if attrValue != "" {
		attr[attrName] = attrValue
	}
}

// Node attributes based on the stereotype
var stereotypeNodeAttributes = map[Stereotype]*nodeAttribute{
	ExternalStereotype: {
		style: "dashed",
	},
}

// Edge attributes based on the reference link
var edgeAttributes = map[LinkType]*edgeAttribute{
	ReferenceLink: {
		arrowHead: "vee",
	},

	AggregationLink: {
		arrowHead: "ediamond",
	},

	InheritanceLink: {
		arrowHead: "onormal",
	},

	RealizationLink: {
		arrowHead: "onormal",
		lineStyle: "dashed",
	},
}

// Stereotype borders based on whether to force the use of 7 bit ASCII
var labelCaps = map[bool]LabelCaps{
	false: {"\u00AB", "\u00BB"},
	true:  {"\\<\\< ", " \\>\\>"},
}
