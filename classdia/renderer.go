package classdia

import (
    "io"
)

// Implements a renderer for a class diagram
type Renderer interface {

    // Renders a diagram to a writer
    RenderTo(diagram *Diagram, w io.Writer) error
}