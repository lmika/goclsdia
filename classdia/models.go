package classdia

import "bytes"

type Diagram struct {
	Classes []*Class
	Links   []*Link
}

type Class struct {
	Stereotype      Stereotype
	StereotypeLabel string
	Name            string
	Parents         []*Class
	Attrs           []*Attr
	Methods         []*Method
}

type Attr struct {
	Visability Visability
	Name       string
	Type       TypeDef
}

// Returns a string representation of the attribute
func (a *Attr) String() string {
	vis := a.Visability.String()
	typeName := a.Type.String()

	var s string
	if vis != "" {
		s = vis + " " + a.Name
	} else {
		s = a.Name
	}

	if typeName != "" {
		s += ": " + typeName
	}

	return s
}

type Method struct {
	Visability Visability
	Name       string
	Args       []*Argument
	Results    []*Argument
}

// Returns a string representation of the method
func (m *Method) String() string {
	vis := m.Visability.String()

	args := &bytes.Buffer{}
	for i, a := range m.Args {
		if i > 0 {
			args.WriteString(", ")
		}
		args.WriteString(a.String())
	}

	var s string
	if vis != "" {
		s = vis + " " + m.Name
	} else {
		s = m.Name
	}

	if args.Len() > 0 {
		s += "(" + args.String() + ")"
	} else {
		s += "()"
	}

	if (len(m.Results) == 1) && (m.Results[0].Name == "") {
		s += ": " + m.Results[0].Type.String()
	} else if len(m.Results) >= 1 {
		rets := &bytes.Buffer{}
		for i, r := range m.Results {
			if i > 0 {
				rets.WriteString(", ")
			}
			rets.WriteString(r.String())
		}
		s += ": (" + rets.String() + ")"
	}

	return s
}

// Argument is an argument or return value of a method.
type Argument struct {
	Name string
	Type TypeDef
}

func (a *Argument) String() string {
	if a.Type.String() != "" {
		return a.Name + ": " + a.Type.String()
	}
	return a.Name
}

type Link struct {
	From      *Class
	To        *Class
	Type      LinkType
	Label     string
	FromLabel string
	ToLabel   string
}

type Visability int

const (
	NoVisability Visability = iota
	PublicVisability
	PrivateVisability
	ProtectedVisability
)

// Returns the string representation of the visability
func (v Visability) String() string {
	switch v {
	case PublicVisability:
		return "+"
	case PrivateVisability:
		return "-"
	case ProtectedVisability:
		return "#"
	default:
		return ""
	}
}

type Stereotype int

const (
	NoStereotype Stereotype = iota
	InterfaceStereotype
	AbstractStereotype
	ExternalStereotype
	OtherStereotype
)

type LinkType int

const (
	// A reference from one class to the other
	ReferenceLink LinkType = iota

	// Aggregation link (from cannot exist without the to)
	AggregationLink

	// Inheritenance link
	InheritanceLink

	// Realization link
	RealizationLink
)

// !!TEMP!!
type TypeDef string

func (t TypeDef) String() string {
	return string(t)
}
