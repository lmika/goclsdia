%{
package classdia

import (
    "io"
    "strconv"
    "text/scanner"
)

var scannerOpts = &scannerOptions {
    TokenRunes: map[rune]int {
        '{': CURL,
        '}': CURR,
        '(': PARL,
        ')': PARR,
        '<': ANGL,
        '>': ANGR,
        '+': PLUS,
        '-': MINUS,
        '#': HASH,
        ':': COLON,
        ',': COMMA,

        // scanner.Int: INT,
        scanner.Ident: IDENT,
        scanner.String: STRING,
    },

    TokenLvals: map[rune]scannerLvalFn {
        /*
        scanner.Int: func(lval *yySymType, s string) (err error) { 
            lval.ival, err = strconv.ParseInt(s, 10, 64)
            return
        },
        */

        scanner.Ident: func(lval *yySymType, s string) (err error) { 
            lval.sval = s
            return
        },

        scanner.String: func(lval *yySymType, s string) (err error) { 
            lval.sval, err = strconv.Unquote(s)
            return
        },
    },

    Keywords: map[string]int {
        "rel": K_REL,
    },
}
%}

%union {
    sval        string
    rval        rune
    pos         scanner.Position

    diagram         *astDiagram
    diagramItem     *astDiagramItem
    diagramItemType astDiagramItemType
    typeDef         *astTypeDef
    typeItem        *astTypeItem
    typeItemType    astTypeItemType
    attribute       *astAttribute
    method          *astMethod
    argItemList     *astArgItemList
    argItem         *astArgItem
    link            *astLink
    stringList      *astStringList
}

%token  CURL CURR
%token  PARL PARR
%token  ANGL ANGR
%token  PLUS MINUS HASH COLON COMMA

%token  K_REL

%token  <sval>  IDENT STRING

%type <diagramItem> diagramitems
%type <diagramItemType> diagramitem
%type <typeDef> typedef
%type <sval> maybestereotype
%type <typeItem> typeitems
%type <typeItemType> typeitem
%type <attribute> attr
%type <method> method
%type <argItemList> arglist mayberetlist argitemlist
%type <argItem> argitem
%type <link> link
%type <stringList> identlist maybeparent
// %type <sval> linktype
%type <sval> identorstring
%type <rval> visabiliy
%type <sval> maybetype type
%type <pos> pos

%%

top
    : diagramitems {
        yylex.(*simpleParserState).parserResult = &astDiagram{$1}
    }
    ;

diagramitems
    : /* empty */ {
        $$ = nil
    }
    | diagramitem diagramitems {
        $$ = &astDiagramItem{$1, $2}
    }
    ;

diagramitem
    : typedef {
        $$ = $1
    }
    ;

typedef
    : pos maybestereotype identorstring maybeparent CURL typeitems CURR {
        $$ = &astTypeDef{$1, $2, $3, "", $4, $6}
    }
    ;

maybeparent
    : /* empty */ {
        $$ = nil
    }
    | COLON identlist {
        $$ = $2;
    }
    ;

maybestereotype
    : /* empty */ {
        $$ = ""
    }
    | ANGL identorstring ANGR {
        $$ = $2
    }
    ;

typeitems
    : /* empty */ {
        $$ = nil
    }
    | typeitem typeitems {
        $$ = &astTypeItem{$1, $2}
    }
    ;

typeitem
    : attr {
        $$ = $1
    }
    | method {
        $$ = $1
    }
    | link {
        $$ = $1
    }
    ;

attr
    : pos visabiliy identorstring maybetype {
        $$ = &astAttribute{$1, $2, $3, $4}
    }
    ;

method
    : pos visabiliy identorstring arglist mayberetlist {
        $$ = &astMethod{$1, $2, $3, $4, $5}
    }
    ;

arglist
    : PARL argitemlist PARR {
        $$ = $2
    }
    ;

mayberetlist
    : /* empty */ {
        $$ = nil
    }
    | pos COLON type {
        $$ = &astArgItemList{&astArgItem{$1, "", $3}, nil}
    }
    | pos COLON arglist {
        // pos here to prevent shift/reduce conflict
        $$ = $3
    }
    ;

argitemlist
    : /* empty */ {
        $$ = nil
    }
    | argitem {
        $$ = &astArgItemList{$1, nil}
    }
    | argitem COMMA argitemlist {
        $$ = &astArgItemList{$1, $3}
    }
    ;

argitem
    : pos identorstring maybetype {
        $$ = &astArgItem{$1, $2, $3}
    }
    | pos COLON type {
        $$ = &astArgItem{$1, "", $3}
    }
    ;

link
    : pos K_REL maybestereotype IDENT identorstring PARL STRING STRING PARR {
        $$ = &astLink{$1, $4, $3, $5, $7, $8}
    }
    | pos K_REL maybestereotype IDENT identorstring {
        $$ = &astLink{$1, $4, $3, $5, "", ""}
    }
    ;

visabiliy
    : /* empty */ {
        $$ = '\x00'
    }
    | PLUS {
        $$ = '+'
    }
    | MINUS {
        $$ = '-'
    }
    | HASH {
        $$ = '#'
    }
    ;

identlist
    : pos identorstring {
        $$ = &astStringList{$1, $2, nil};
    }
    | pos identorstring COLON identlist {
        $$ = &astStringList{$1, $2, $4};
    }
    ;

maybetype
    : /* empty */ {
        $$ = ""
    }
    | COLON type {
        $$ = $2;
    }
    ;

/* TEMP: Just strings for the moment */
type
    : identorstring {
        $$ = $1;
    }
    ;

identorstring
    : IDENT {
        $$ = $1
    }
    | STRING {
        $$ = $1
    }
    ;

pos
    : /* position snapshot */ {
        $$ = yylex.(*simpleParserState).Pos()
    }
    ;

%%

func parse(reader io.Reader, filename string) (*astDiagram, error) {
    ps := newSimpleParserState(reader, filename, scannerOpts)
    yyParse(ps)

    if ps.err != nil {
        return nil, ps.err
    } else {
        return ps.parserResult.(*astDiagram), nil
    }
}