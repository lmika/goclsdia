package classdia

// StripStereotypeOption is a test option which will strip the stereotypes
// from a diagram.
type StripStereotypeOption struct {
}

// FilterDiagram strips the stereotypes of the diagram.
func (ss *StripStereotypeOption) FilterDiagram(diagram *Diagram) (*Diagram, error) {
	for _, cls := range diagram.Classes {
		cls.Stereotype = NoStereotype
		cls.StereotypeLabel = ""
	}

	return diagram, nil
}
