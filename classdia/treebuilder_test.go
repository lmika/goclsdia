package classdia

import (
	"strings"
	"testing"

	. "github.com/seanpont/assert"
)

func TestParser1(t *testing.T) {
	assert := Assert(t)

	d := parseDiagram(assert, `Class1 { }  Class2 { }`)

	assert.Equal(len(d.Classes), 2)
	assert.Equal(d.Classes[0].Name, "Class1")
	assert.Equal(d.Classes[1].Name, "Class2")
}

func TestParserAttr(t *testing.T) {
	assert := Assert(t)

	d := parseDiagram(assert, `Class1 { 
        alpha
        bravo
        charlie 
    }`)

	assert.Equal(len(d.Classes), 1)
	assert.Equal(d.Classes[0].Name, "Class1")
	assert.Equal(d.Classes[0].Attrs[0], &Attr{Visability: NoVisability, Name: "alpha", Type: ""})
	assert.Equal(d.Classes[0].Attrs[1], &Attr{Visability: NoVisability, Name: "bravo", Type: ""})
	assert.Equal(d.Classes[0].Attrs[2], &Attr{Visability: NoVisability, Name: "charlie", Type: ""})
}

func TestParserAttr2(t *testing.T) {
	assert := Assert(t)

	d := parseDiagram(assert, `Class1 { 
        + alpha: string
        - bravo: int
        # charlie: bool
    }`)

	assert.Equal(len(d.Classes), 1)
	assert.Equal(d.Classes[0].Name, "Class1")
	assert.Equal(d.Classes[0].Attrs[0], &Attr{Visability: PublicVisability, Name: "alpha", Type: "string"})
	assert.Equal(d.Classes[0].Attrs[1], &Attr{Visability: PrivateVisability, Name: "bravo", Type: "int"})
	assert.Equal(d.Classes[0].Attrs[2], &Attr{Visability: ProtectedVisability, Name: "charlie", Type: "bool"})
}

func TestParserAttr3(t *testing.T) {
	assert := Assert(t)

	d := parseDiagram(assert, `"Class1 name" { 
        + "alpha value": string
        - bravo: "int type"
        # charlie: bool
    }`)

	assert.Equal(len(d.Classes), 1)
	assert.Equal(d.Classes[0].Name, "Class1 name")
	assert.Equal(d.Classes[0].Attrs[0], &Attr{Visability: PublicVisability, Name: "alpha value", Type: "string"})
	assert.Equal(d.Classes[0].Attrs[1], &Attr{Visability: PrivateVisability, Name: "bravo", Type: "int type"})
	assert.Equal(d.Classes[0].Attrs[2], &Attr{Visability: ProtectedVisability, Name: "charlie", Type: "bool"})
}

func TestParseMethods(t *testing.T) {
	assert := Assert(t)

	d := parseDiagram(assert, `Class1 { 
        test()
    }`)

	assert.Equal(len(d.Classes), 1)
	assert.Equal(d.Classes[0].Name, "Class1")
	assert.Equal(d.Classes[0].Methods[0], &Method{
		Visability: NoVisability,
		Name:       "test",
		Args:       []*Argument{},
		Results:    []*Argument{},
	})
}

func TestParseMethods2(t *testing.T) {
	assert := Assert(t)

	d := parseDiagram(assert, `Class1 { 
        + sum(x: int, y: int): int
    }`)

	assert.Equal(len(d.Classes), 1)
	assert.Equal(d.Classes[0].Name, "Class1")
	assert.Equal(d.Classes[0].Methods[0], &Method{
		Visability: PublicVisability,
		Name:       "sum",
		Args: []*Argument{
			&Argument{Name: "x", Type: "int"},
			&Argument{Name: "y", Type: "int"},
		},
		Results: []*Argument{
			&Argument{Name: "", Type: "int"},
		},
	})
}

func TestParseMethods3(t *testing.T) {
	assert := Assert(t)

	d := parseDiagram(assert, `Class1 { 
        + sum(:int, :int): int
    }`)

	assert.Equal(len(d.Classes), 1)
	assert.Equal(d.Classes[0].Name, "Class1")
	assert.Equal(d.Classes[0].Methods[0], &Method{
		Visability: PublicVisability,
		Name:       "sum",
		Args: []*Argument{
			&Argument{Name: "", Type: "int"},
			&Argument{Name: "", Type: "int"},
		},
		Results: []*Argument{
			&Argument{Name: "", Type: "int"},
		},
	})
}

func TestParseMethods4(t *testing.T) {
	assert := Assert(t)

	d := parseDiagram(assert, `Class1 { 
        + put(key: string, value: "item<object>"): (:int, :"item<object>")
    }`)

	assert.Equal(len(d.Classes), 1)
	assert.Equal(d.Classes[0].Name, "Class1")
	assert.Equal(d.Classes[0].Methods[0], &Method{
		Visability: PublicVisability,
		Name:       "put",
		Args: []*Argument{
			&Argument{Name: "key", Type: "string"},
			&Argument{Name: "value", Type: "item<object>"},
		},
		Results: []*Argument{
			&Argument{Name: "", Type: "int"},
			&Argument{Name: "", Type: "item<object>"},
		},
	})
}

func TestParseMethods5(t *testing.T) {
	assert := Assert(t)

	d := parseDiagram(assert, `Class1 { 
        + put(:string, newValue: "item<object>"): (:int, oldValue: "item<object>")
    }`)

	assert.Equal(len(d.Classes), 1)
	assert.Equal(d.Classes[0].Name, "Class1")
	assert.Equal(d.Classes[0].Methods[0], &Method{
		Visability: PublicVisability,
		Name:       "put",
		Args: []*Argument{
			&Argument{Name: "", Type: "string"},
			&Argument{Name: "newValue", Type: "item<object>"},
		},
		Results: []*Argument{
			&Argument{Name: "", Type: "int"},
			&Argument{Name: "oldValue", Type: "item<object>"},
		},
	})
}

func TestStereotype(t *testing.T) {
	assert := Assert(t)

	d := parseDiagram(assert, `
        <interface> Something { }
        <abstract> "Something Else" { }
        <external> "Externally" { }`)

	assert.Equal(len(d.Classes), 3)
	assert.Equal(d.Classes[0].Name, "Something")
	assert.Equal(d.Classes[0].Stereotype, InterfaceStereotype)
	assert.Equal(d.Classes[0].StereotypeLabel, "interface")
	assert.Equal(d.Classes[1].Name, "Something Else")
	assert.Equal(d.Classes[1].Stereotype, AbstractStereotype)
	assert.Equal(d.Classes[1].StereotypeLabel, "abstract")
	assert.Equal(d.Classes[2].Name, "Externally")
	assert.Equal(d.Classes[2].Stereotype, ExternalStereotype)
	assert.Equal(d.Classes[2].StereotypeLabel, "external")
}

func parseDiagram(assert *Assertion, s string) *Diagram {
	diagram, err := Parse(strings.NewReader(s), "string")

	assert.Nil(err)
	return diagram
}
