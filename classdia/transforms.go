package classdia

// DiagramFilter applies a specific transformation over the specific diagram.
type DiagramFilter interface {

	// Applies a filter to the diagram.  This returns the filtered diagram (which
	// could be the same diagram) or an error.
	FilterDiagram(diagram *Diagram) (*Diagram, error)
}
