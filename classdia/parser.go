//line parser.y:2
package classdia

import __yyfmt__ "fmt"

//line parser.y:2
import (
	"io"
	"strconv"
	"text/scanner"
)

var scannerOpts = &scannerOptions{
	TokenRunes: map[rune]int{
		'{': CURL,
		'}': CURR,
		'(': PARL,
		')': PARR,
		'<': ANGL,
		'>': ANGR,
		'+': PLUS,
		'-': MINUS,
		'#': HASH,
		':': COLON,
		',': COMMA,

		// scanner.Int: INT,
		scanner.Ident:  IDENT,
		scanner.String: STRING,
	},

	TokenLvals: map[rune]scannerLvalFn{
		/*
		   scanner.Int: func(lval *yySymType, s string) (err error) {
		       lval.ival, err = strconv.ParseInt(s, 10, 64)
		       return
		   },
		*/

		scanner.Ident: func(lval *yySymType, s string) (err error) {
			lval.sval = s
			return
		},

		scanner.String: func(lval *yySymType, s string) (err error) {
			lval.sval, err = strconv.Unquote(s)
			return
		},
	},

	Keywords: map[string]int{
		"rel": K_REL,
	},
}

//line parser.y:54
type yySymType struct {
	yys  int
	sval string
	rval rune
	pos  scanner.Position

	diagram         *astDiagram
	diagramItem     *astDiagramItem
	diagramItemType astDiagramItemType
	typeDef         *astTypeDef
	typeItem        *astTypeItem
	typeItemType    astTypeItemType
	attribute       *astAttribute
	method          *astMethod
	argItemList     *astArgItemList
	argItem         *astArgItem
	link            *astLink
	stringList      *astStringList
}

const CURL = 57346
const CURR = 57347
const PARL = 57348
const PARR = 57349
const ANGL = 57350
const ANGR = 57351
const PLUS = 57352
const MINUS = 57353
const HASH = 57354
const COLON = 57355
const COMMA = 57356
const K_REL = 57357
const IDENT = 57358
const STRING = 57359

var yyToknames = [...]string{
	"$end",
	"error",
	"$unk",
	"CURL",
	"CURR",
	"PARL",
	"PARR",
	"ANGL",
	"ANGR",
	"PLUS",
	"MINUS",
	"HASH",
	"COLON",
	"COMMA",
	"K_REL",
	"IDENT",
	"STRING",
}
var yyStatenames = [...]string{}

const yyEofCode = 1
const yyErrCode = 2
const yyInitialStackSize = 16

//line parser.y:284

func parse(reader io.Reader, filename string) (*astDiagram, error) {
	ps := newSimpleParserState(reader, filename, scannerOpts)
	yyParse(ps)

	if ps.err != nil {
		return nil, ps.err
	} else {
		return ps.parserResult.(*astDiagram), nil
	}
}

//line yacctab:1
var yyExca = [...]int{
	-1, 0,
	1, 2,
	-2, 39,
	-1, 1,
	1, -1,
	-2, 0,
	-1, 3,
	1, 2,
	-2, 39,
	-1, 16,
	5, 10,
	-2, 39,
	-1, 20,
	5, 10,
	-2, 39,
	-1, 38,
	13, 39,
	-2, 18,
	-1, 40,
	7, 21,
	-2, 39,
	-1, 52,
	7, 21,
	-2, 39,
}

const yyNprod = 40
const yyPrivate = 57344

var yyTokenNames []string
var yyStates []string

const yyLast = 72

var yyAct = [...]int{

	5, 44, 46, 37, 17, 19, 7, 54, 38, 40,
	10, 11, 10, 11, 62, 18, 45, 24, 61, 10,
	11, 24, 41, 52, 9, 12, 27, 30, 31, 32,
	40, 39, 29, 50, 18, 25, 35, 39, 36, 43,
	33, 48, 14, 15, 8, 34, 63, 51, 55, 26,
	16, 1, 56, 48, 28, 58, 60, 59, 49, 57,
	2, 13, 23, 47, 6, 53, 42, 22, 21, 20,
	4, 3,
}
var yyPact = [...]int{

	-1000, -1000, -1000, -1000, -1000, 36, -1000, -4, -4, 29,
	-1000, -1000, 34, 46, -1000, -1000, -1000, -1000, -4, 44,
	-1000, -1000, -1000, -1000, 17, 27, -1000, -1000, -4, 36,
	-1000, -1000, -1000, -1000, 24, 6, -1000, -1000, -1000, -4,
	-1000, -4, -1000, 20, -1000, -1000, 40, 9, -6, 42,
	3, -1000, -1000, 18, -4, 1, -1000, -1000, -1000, -1000,
	-1000, -3, 39, -1000,
}
var yyPgo = [...]int{

	0, 60, 71, 70, 6, 5, 69, 68, 67, 8,
	66, 2, 63, 62, 4, 61, 16, 54, 3, 1,
	0, 51,
}
var yyR1 = [...]int{

	0, 21, 1, 1, 2, 3, 15, 15, 4, 4,
	5, 5, 6, 6, 6, 7, 8, 9, 10, 10,
	10, 11, 11, 11, 12, 12, 13, 13, 17, 17,
	17, 17, 14, 14, 18, 18, 19, 16, 16, 20,
}
var yyR2 = [...]int{

	0, 1, 0, 2, 1, 7, 0, 2, 0, 3,
	0, 2, 1, 1, 1, 4, 5, 3, 0, 3,
	3, 0, 1, 3, 3, 3, 9, 5, 0, 1,
	1, 1, 2, 4, 0, 2, 1, 1, 1, 0,
}
var yyChk = [...]int{

	-1000, -21, -1, -2, -3, -20, -1, -4, 8, -16,
	16, 17, -16, -15, 13, 9, 4, -14, -20, -5,
	-6, -7, -8, -13, -20, -16, 5, -5, -17, 15,
	10, 11, 12, 13, -16, -4, -14, -18, -9, 13,
	6, 16, -10, -20, -19, -16, -11, -12, -20, -16,
	13, 7, 14, -16, 13, 6, -19, -9, -11, -18,
	-19, 17, 17, 7,
}
var yyDef = [...]int{

	-2, -2, 1, -2, 4, 8, 3, 0, 0, 6,
	37, 38, 0, 0, 39, 9, -2, 7, 0, 0,
	-2, 12, 13, 14, 28, 32, 5, 11, 0, 8,
	29, 30, 31, 39, 34, 0, 33, 15, -2, 0,
	-2, 0, 16, 0, 35, 36, 0, 22, 0, 27,
	0, 17, -2, 34, 0, 0, 19, 20, 23, 24,
	25, 0, 0, 26,
}
var yyTok1 = [...]int{

	1,
}
var yyTok2 = [...]int{

	2, 3, 4, 5, 6, 7, 8, 9, 10, 11,
	12, 13, 14, 15, 16, 17,
}
var yyTok3 = [...]int{
	0,
}

var yyErrorMessages = [...]struct {
	state int
	token int
	msg   string
}{}

//line yaccpar:1

/*	parser for yacc output	*/

var (
	yyDebug        = 0
	yyErrorVerbose = false
)

type yyLexer interface {
	Lex(lval *yySymType) int
	Error(s string)
}

type yyParser interface {
	Parse(yyLexer) int
	Lookahead() int
}

type yyParserImpl struct {
	lval  yySymType
	stack [yyInitialStackSize]yySymType
	char  int
}

func (p *yyParserImpl) Lookahead() int {
	return p.char
}

func yyNewParser() yyParser {
	return &yyParserImpl{}
}

const yyFlag = -1000

func yyTokname(c int) string {
	if c >= 1 && c-1 < len(yyToknames) {
		if yyToknames[c-1] != "" {
			return yyToknames[c-1]
		}
	}
	return __yyfmt__.Sprintf("tok-%v", c)
}

func yyStatname(s int) string {
	if s >= 0 && s < len(yyStatenames) {
		if yyStatenames[s] != "" {
			return yyStatenames[s]
		}
	}
	return __yyfmt__.Sprintf("state-%v", s)
}

func yyErrorMessage(state, lookAhead int) string {
	const TOKSTART = 4

	if !yyErrorVerbose {
		return "syntax error"
	}

	for _, e := range yyErrorMessages {
		if e.state == state && e.token == lookAhead {
			return "syntax error: " + e.msg
		}
	}

	res := "syntax error: unexpected " + yyTokname(lookAhead)

	// To match Bison, suggest at most four expected tokens.
	expected := make([]int, 0, 4)

	// Look for shiftable tokens.
	base := yyPact[state]
	for tok := TOKSTART; tok-1 < len(yyToknames); tok++ {
		if n := base + tok; n >= 0 && n < yyLast && yyChk[yyAct[n]] == tok {
			if len(expected) == cap(expected) {
				return res
			}
			expected = append(expected, tok)
		}
	}

	if yyDef[state] == -2 {
		i := 0
		for yyExca[i] != -1 || yyExca[i+1] != state {
			i += 2
		}

		// Look for tokens that we accept or reduce.
		for i += 2; yyExca[i] >= 0; i += 2 {
			tok := yyExca[i]
			if tok < TOKSTART || yyExca[i+1] == 0 {
				continue
			}
			if len(expected) == cap(expected) {
				return res
			}
			expected = append(expected, tok)
		}

		// If the default action is to accept or reduce, give up.
		if yyExca[i+1] != 0 {
			return res
		}
	}

	for i, tok := range expected {
		if i == 0 {
			res += ", expecting "
		} else {
			res += " or "
		}
		res += yyTokname(tok)
	}
	return res
}

func yylex1(lex yyLexer, lval *yySymType) (char, token int) {
	token = 0
	char = lex.Lex(lval)
	if char <= 0 {
		token = yyTok1[0]
		goto out
	}
	if char < len(yyTok1) {
		token = yyTok1[char]
		goto out
	}
	if char >= yyPrivate {
		if char < yyPrivate+len(yyTok2) {
			token = yyTok2[char-yyPrivate]
			goto out
		}
	}
	for i := 0; i < len(yyTok3); i += 2 {
		token = yyTok3[i+0]
		if token == char {
			token = yyTok3[i+1]
			goto out
		}
	}

out:
	if token == 0 {
		token = yyTok2[1] /* unknown char */
	}
	if yyDebug >= 3 {
		__yyfmt__.Printf("lex %s(%d)\n", yyTokname(token), uint(char))
	}
	return char, token
}

func yyParse(yylex yyLexer) int {
	return yyNewParser().Parse(yylex)
}

func (yyrcvr *yyParserImpl) Parse(yylex yyLexer) int {
	var yyn int
	var yyVAL yySymType
	var yyDollar []yySymType
	_ = yyDollar // silence set and not used
	yyS := yyrcvr.stack[:]

	Nerrs := 0   /* number of errors */
	Errflag := 0 /* error recovery flag */
	yystate := 0
	yyrcvr.char = -1
	yytoken := -1 // yyrcvr.char translated into internal numbering
	defer func() {
		// Make sure we report no lookahead when not parsing.
		yystate = -1
		yyrcvr.char = -1
		yytoken = -1
	}()
	yyp := -1
	goto yystack

ret0:
	return 0

ret1:
	return 1

yystack:
	/* put a state and value onto the stack */
	if yyDebug >= 4 {
		__yyfmt__.Printf("char %v in %v\n", yyTokname(yytoken), yyStatname(yystate))
	}

	yyp++
	if yyp >= len(yyS) {
		nyys := make([]yySymType, len(yyS)*2)
		copy(nyys, yyS)
		yyS = nyys
	}
	yyS[yyp] = yyVAL
	yyS[yyp].yys = yystate

yynewstate:
	yyn = yyPact[yystate]
	if yyn <= yyFlag {
		goto yydefault /* simple state */
	}
	if yyrcvr.char < 0 {
		yyrcvr.char, yytoken = yylex1(yylex, &yyrcvr.lval)
	}
	yyn += yytoken
	if yyn < 0 || yyn >= yyLast {
		goto yydefault
	}
	yyn = yyAct[yyn]
	if yyChk[yyn] == yytoken { /* valid shift */
		yyrcvr.char = -1
		yytoken = -1
		yyVAL = yyrcvr.lval
		yystate = yyn
		if Errflag > 0 {
			Errflag--
		}
		goto yystack
	}

yydefault:
	/* default state action */
	yyn = yyDef[yystate]
	if yyn == -2 {
		if yyrcvr.char < 0 {
			yyrcvr.char, yytoken = yylex1(yylex, &yyrcvr.lval)
		}

		/* look through exception table */
		xi := 0
		for {
			if yyExca[xi+0] == -1 && yyExca[xi+1] == yystate {
				break
			}
			xi += 2
		}
		for xi += 2; ; xi += 2 {
			yyn = yyExca[xi+0]
			if yyn < 0 || yyn == yytoken {
				break
			}
		}
		yyn = yyExca[xi+1]
		if yyn < 0 {
			goto ret0
		}
	}
	if yyn == 0 {
		/* error ... attempt to resume parsing */
		switch Errflag {
		case 0: /* brand new error */
			yylex.Error(yyErrorMessage(yystate, yytoken))
			Nerrs++
			if yyDebug >= 1 {
				__yyfmt__.Printf("%s", yyStatname(yystate))
				__yyfmt__.Printf(" saw %s\n", yyTokname(yytoken))
			}
			fallthrough

		case 1, 2: /* incompletely recovered error ... try again */
			Errflag = 3

			/* find a state where "error" is a legal shift action */
			for yyp >= 0 {
				yyn = yyPact[yyS[yyp].yys] + yyErrCode
				if yyn >= 0 && yyn < yyLast {
					yystate = yyAct[yyn] /* simulate a shift of "error" */
					if yyChk[yystate] == yyErrCode {
						goto yystack
					}
				}

				/* the current p has no shift on "error", pop stack */
				if yyDebug >= 2 {
					__yyfmt__.Printf("error recovery pops state %d\n", yyS[yyp].yys)
				}
				yyp--
			}
			/* there is no state on the stack with an error shift ... abort */
			goto ret1

		case 3: /* no shift yet; clobber input char */
			if yyDebug >= 2 {
				__yyfmt__.Printf("error recovery discards %s\n", yyTokname(yytoken))
			}
			if yytoken == yyEofCode {
				goto ret1
			}
			yyrcvr.char = -1
			yytoken = -1
			goto yynewstate /* try again in the same state */
		}
	}

	/* reduction by production yyn */
	if yyDebug >= 2 {
		__yyfmt__.Printf("reduce %v in:\n\t%v\n", yyn, yyStatname(yystate))
	}

	yynt := yyn
	yypt := yyp
	_ = yypt // guard against "declared and not used"

	yyp -= yyR2[yyn]
	// yyp is now the index of $0. Perform the default action. Iff the
	// reduced production is ε, $1 is possibly out of range.
	if yyp+1 >= len(yyS) {
		nyys := make([]yySymType, len(yyS)*2)
		copy(nyys, yyS)
		yyS = nyys
	}
	yyVAL = yyS[yyp+1]

	/* consult goto table to find next state */
	yyn = yyR1[yyn]
	yyg := yyPgo[yyn]
	yyj := yyg + yyS[yyp].yys + 1

	if yyj >= yyLast {
		yystate = yyAct[yyg]
	} else {
		yystate = yyAct[yyj]
		if yyChk[yystate] != -yyn {
			yystate = yyAct[yyg]
		}
	}
	// dummy call; replaced with literal code
	switch yynt {

	case 1:
		yyDollar = yyS[yypt-1 : yypt+1]
		//line parser.y:103
		{
			yylex.(*simpleParserState).parserResult = &astDiagram{yyDollar[1].diagramItem}
		}
	case 2:
		yyDollar = yyS[yypt-0 : yypt+1]
		//line parser.y:109
		{
			yyVAL.diagramItem = nil
		}
	case 3:
		yyDollar = yyS[yypt-2 : yypt+1]
		//line parser.y:112
		{
			yyVAL.diagramItem = &astDiagramItem{yyDollar[1].diagramItemType, yyDollar[2].diagramItem}
		}
	case 4:
		yyDollar = yyS[yypt-1 : yypt+1]
		//line parser.y:118
		{
			yyVAL.diagramItemType = yyDollar[1].typeDef
		}
	case 5:
		yyDollar = yyS[yypt-7 : yypt+1]
		//line parser.y:124
		{
			yyVAL.typeDef = &astTypeDef{yyDollar[1].pos, yyDollar[2].sval, yyDollar[3].sval, "", yyDollar[4].stringList, yyDollar[6].typeItem}
		}
	case 6:
		yyDollar = yyS[yypt-0 : yypt+1]
		//line parser.y:130
		{
			yyVAL.stringList = nil
		}
	case 7:
		yyDollar = yyS[yypt-2 : yypt+1]
		//line parser.y:133
		{
			yyVAL.stringList = yyDollar[2].stringList
		}
	case 8:
		yyDollar = yyS[yypt-0 : yypt+1]
		//line parser.y:139
		{
			yyVAL.sval = ""
		}
	case 9:
		yyDollar = yyS[yypt-3 : yypt+1]
		//line parser.y:142
		{
			yyVAL.sval = yyDollar[2].sval
		}
	case 10:
		yyDollar = yyS[yypt-0 : yypt+1]
		//line parser.y:148
		{
			yyVAL.typeItem = nil
		}
	case 11:
		yyDollar = yyS[yypt-2 : yypt+1]
		//line parser.y:151
		{
			yyVAL.typeItem = &astTypeItem{yyDollar[1].typeItemType, yyDollar[2].typeItem}
		}
	case 12:
		yyDollar = yyS[yypt-1 : yypt+1]
		//line parser.y:157
		{
			yyVAL.typeItemType = yyDollar[1].attribute
		}
	case 13:
		yyDollar = yyS[yypt-1 : yypt+1]
		//line parser.y:160
		{
			yyVAL.typeItemType = yyDollar[1].method
		}
	case 14:
		yyDollar = yyS[yypt-1 : yypt+1]
		//line parser.y:163
		{
			yyVAL.typeItemType = yyDollar[1].link
		}
	case 15:
		yyDollar = yyS[yypt-4 : yypt+1]
		//line parser.y:169
		{
			yyVAL.attribute = &astAttribute{yyDollar[1].pos, yyDollar[2].rval, yyDollar[3].sval, yyDollar[4].sval}
		}
	case 16:
		yyDollar = yyS[yypt-5 : yypt+1]
		//line parser.y:175
		{
			yyVAL.method = &astMethod{yyDollar[1].pos, yyDollar[2].rval, yyDollar[3].sval, yyDollar[4].argItemList, yyDollar[5].argItemList}
		}
	case 17:
		yyDollar = yyS[yypt-3 : yypt+1]
		//line parser.y:181
		{
			yyVAL.argItemList = yyDollar[2].argItemList
		}
	case 18:
		yyDollar = yyS[yypt-0 : yypt+1]
		//line parser.y:187
		{
			yyVAL.argItemList = nil
		}
	case 19:
		yyDollar = yyS[yypt-3 : yypt+1]
		//line parser.y:190
		{
			yyVAL.argItemList = &astArgItemList{&astArgItem{yyDollar[1].pos, "", yyDollar[3].sval}, nil}
		}
	case 20:
		yyDollar = yyS[yypt-3 : yypt+1]
		//line parser.y:193
		{
			// pos here to prevent shift/reduce conflict
			yyVAL.argItemList = yyDollar[3].argItemList
		}
	case 21:
		yyDollar = yyS[yypt-0 : yypt+1]
		//line parser.y:200
		{
			yyVAL.argItemList = nil
		}
	case 22:
		yyDollar = yyS[yypt-1 : yypt+1]
		//line parser.y:203
		{
			yyVAL.argItemList = &astArgItemList{yyDollar[1].argItem, nil}
		}
	case 23:
		yyDollar = yyS[yypt-3 : yypt+1]
		//line parser.y:206
		{
			yyVAL.argItemList = &astArgItemList{yyDollar[1].argItem, yyDollar[3].argItemList}
		}
	case 24:
		yyDollar = yyS[yypt-3 : yypt+1]
		//line parser.y:212
		{
			yyVAL.argItem = &astArgItem{yyDollar[1].pos, yyDollar[2].sval, yyDollar[3].sval}
		}
	case 25:
		yyDollar = yyS[yypt-3 : yypt+1]
		//line parser.y:215
		{
			yyVAL.argItem = &astArgItem{yyDollar[1].pos, "", yyDollar[3].sval}
		}
	case 26:
		yyDollar = yyS[yypt-9 : yypt+1]
		//line parser.y:221
		{
			yyVAL.link = &astLink{yyDollar[1].pos, yyDollar[4].sval, yyDollar[3].sval, yyDollar[5].sval, yyDollar[7].sval, yyDollar[8].sval}
		}
	case 27:
		yyDollar = yyS[yypt-5 : yypt+1]
		//line parser.y:224
		{
			yyVAL.link = &astLink{yyDollar[1].pos, yyDollar[4].sval, yyDollar[3].sval, yyDollar[5].sval, "", ""}
		}
	case 28:
		yyDollar = yyS[yypt-0 : yypt+1]
		//line parser.y:230
		{
			yyVAL.rval = '\x00'
		}
	case 29:
		yyDollar = yyS[yypt-1 : yypt+1]
		//line parser.y:233
		{
			yyVAL.rval = '+'
		}
	case 30:
		yyDollar = yyS[yypt-1 : yypt+1]
		//line parser.y:236
		{
			yyVAL.rval = '-'
		}
	case 31:
		yyDollar = yyS[yypt-1 : yypt+1]
		//line parser.y:239
		{
			yyVAL.rval = '#'
		}
	case 32:
		yyDollar = yyS[yypt-2 : yypt+1]
		//line parser.y:245
		{
			yyVAL.stringList = &astStringList{yyDollar[1].pos, yyDollar[2].sval, nil}
		}
	case 33:
		yyDollar = yyS[yypt-4 : yypt+1]
		//line parser.y:248
		{
			yyVAL.stringList = &astStringList{yyDollar[1].pos, yyDollar[2].sval, yyDollar[4].stringList}
		}
	case 34:
		yyDollar = yyS[yypt-0 : yypt+1]
		//line parser.y:254
		{
			yyVAL.sval = ""
		}
	case 35:
		yyDollar = yyS[yypt-2 : yypt+1]
		//line parser.y:257
		{
			yyVAL.sval = yyDollar[2].sval
		}
	case 36:
		yyDollar = yyS[yypt-1 : yypt+1]
		//line parser.y:264
		{
			yyVAL.sval = yyDollar[1].sval
		}
	case 37:
		yyDollar = yyS[yypt-1 : yypt+1]
		//line parser.y:270
		{
			yyVAL.sval = yyDollar[1].sval
		}
	case 38:
		yyDollar = yyS[yypt-1 : yypt+1]
		//line parser.y:273
		{
			yyVAL.sval = yyDollar[1].sval
		}
	case 39:
		yyDollar = yyS[yypt-0 : yypt+1]
		//line parser.y:279
		{
			yyVAL.pos = yylex.(*simpleParserState).Pos()
		}
	}
	goto yystack /* stack new state and value */
}
