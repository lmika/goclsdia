package classdia

import (
    "testing"
    "strings"

 .  "github.com/seanpont/assert"

    "github.com/awalterschulze/gographviz"
)

func TestEscapedStrings(t *testing.T) {
    assert := Assert(t)

    graph, err := renderGoGraphvis(`Class { "<angleBrackets>" "{curlyBrackets}" }`)
    graph2, err2 := renderGoGraphvis(`"<class>" { "|bars|" "so called \"quotes\"" }`)

    assert.Nil(err)
    assert.Equal(graph.Nodes.Lookup["Class"].Attrs["label"], "\"{ Class | \\<angleBrackets\\>\\n\\{curlyBrackets\\} }\"")

    assert.Nil(err2)
    assert.Equal(graph2.Nodes.Lookup["<class>"].Attrs["label"], "\"{ \\<class\\> | \\|bars\\|\\nso called \\\"quotes\\\" }\"")
}

func TestStereoType(t *testing.T) {
    assert := Assert(t)

    graph, err := renderGoGraphvis(`<interface> Class { }`)
    graph2, err2 := renderGoGraphvis(`<"another interface"> Type { }`)

    assert.Nil(err)
    assert.Equal(graph.Nodes.Lookup["Class"].Attrs["label"], "\"{ \u00ABinterface\u00BB\\nClass }\"")

    assert.Nil(err2)
    assert.Equal(graph2.Nodes.Lookup["Type"].Attrs["label"], "\"{ \u00ABanother interface\u00BB\\nType }\"")
}

func renderGoGraphvis(spec string) (*gographviz.Graph, error) {
    diagram, err := Parse(strings.NewReader(spec), "stdin")
    if err != nil {
        return nil, err
    }

    di := &dotRenderInstance{
        graph: gographviz.NewGraph(),
        diagram: diagram,
        font: "Helvetica-Narrow",
        orientationAttributes: orientationAttributesMap[Vertical],
        stereotypeCaps: labelCaps[false],
        edgeLabelCaps: labelCaps[false],
    }

    err = di.doRender()
    if err != nil {
        return nil, err
    }

    return di.graph, nil
}