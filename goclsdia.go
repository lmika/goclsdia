package main

import (
	"flag"
	"log"
	"os"

	"bitbucket.org/lmika/goclsdia/classdia"
)

var horizontal *bool = flag.Bool("H", false, "arrange the class diagram horizontally")

var useAscii *bool = flag.Bool("7", false, "use 7-bit ascii for text decorations")

func main() {
	flag.Parse()

	diagram, err := classdia.Parse(os.Stdin, "stdin")
	if err != nil {
		log.Fatal(err)
	}

	// TEMP
	// diagram, err = new(classdia.StripStereotypeOption).FilterDiagram(diagram)
	// if err != nil {
	// 	log.Fatal(err)
	// }
	// END TEMP

	renderer := &classdia.DotRenderer{
		Font:       "Helvetica-Narrow",
		ForceAscii: *useAscii,
	}
	if *horizontal {
		renderer.Orientation = classdia.Horizontal
	}

	renderer.RenderTo(diagram, os.Stdout)
}
