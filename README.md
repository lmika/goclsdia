goclsdia - UML Class Diagram Generator
======================================

This tool generates [dot](http://graphviz.org/) files representing UML
class diagrams using text-based description files.

**NOTE**: Language generation is in progress.

Usage
-----

Spec File
---------


### Formal Gramma

The gramma of the spec file is as follows:

    diagram     ->  type*

    type        ->  alias? typename supertypes properties
    alias       ->  IDENT "="
    typename    ->  identOrString
    supertypes  ->  ":" identOrString ("," identOrString)*

    properties  ->  (attribute | method)*

    attribute   ->  visability identOrString typedef?

    method      ->  visability identOrString arguments results?
    arguments   ->  "(" argument ("," argument) ")"
    argument    ->  identOrString? typedef? | "_" | "..."
    results     ->  typedef | arguments

    visability  ->  "+" | "-" | "#"

    typedef     ->  ":" type

    type        ->  IDENT       // TODO

    identOrString -> IDENT | STRING




### Types

Types are defined by including the type name, followed by the curly brackets:

    Carpark {
    }

Types can have stereotypes, which are added before the type name in angle brackets:

    <interface> MultiStoried {
        + levels: int
    }

TODO - If the type is to extend or implements any classes or interfaces, they can be
added after the name using a colon ':'

    Building {

    }

    <interface> MultiStoried {

    }

    Carpark: Building, CarStorable {

    }

TODO - A type can also have an alias, which can be defined before the type name using
the equals sign.  The alias can be used in place of the type's name:

    structure = Building {        
    }

    highRise = <interface> MultiStoried {

    }

    Carpark: structure, highRise {

    }

### Attributes

Attributes are defined by declaring them within the curly brackets:

    Carpark {
        + capacity: int
        + address: string
        - opened: boolean
    }

Attributes for a type are defined using the following format:

    visability? attributeName (":" attributeType)?

The visability and types are optional:

    Carpark {
        capacity
        address
        opened
    }

Attribute names can also be strings if they contain spaces or reserved words:

    Carpark {
        "numeber of cars"
        "interface"        
    }

### Methods

Methods are similar to attributes except they must have brackets following the name:

    Carpark {
        + enterCarpark(car: Car)
        - exitCarpark(): Car
    }

Either the parameter name or type name can be omitted, but not both.  The method return
type can be omitted as well:

    Carpark {
        + enterCarpark(:Car, :int)
        + enterCarpark(car, occupants)
    }

Methods can return multiple arguments.  To specify multiple return values, or to name
multiple return values, the return values need to be surrounded by brackets:

    Carpark {
        + capacity(): (freeSpaces: int, usedSpaces: int)
    }

Like arguments, when using multiple return values; either the name or the type can
be omitted, but not both.  When omitting the names, types should be prefixed with
a colon:

    Carpark {
        + capacity(): (:int, :int)
    }

TODO - however, to allow for the common case, the `result_names_as_types` modifier can be used
to use the return argument name as the type if the type is not specified:

    // NOTE: not implemented yet.  Might be enabled by default.
    use result_names_as_types

    Carpark {
        + capacity(): (int, int)  // will be treated as: (:int, :int)
    }

### Explicit Associations

Relationships between two classes can be explicitly defined using the `rel` keyword.  The
general format of a relationship is:

    rel <relationship-type> <other-class>

The relationship is defined on the source type:

    Carpark {
        contains: Cars
    }

    Cars {
        color: string
        drivenBy: Driver

        rel aggregate_of Carpark
        rel references Driver
    }

    Driver {
        name: string
    }

TODO - Multiple relationships can be defined within a block using parenthesis:

    Carpark {
        contains: Cars
    }

    Cars {
        color: string
        drivenBy: Driver

        rel (
            aggregate_of Carpark
            references Driver
        )
    }

    Driver {
        name: string
    }



Some examples of relationships:

- `references`: this type makes use of this other type
- `aggregate_of`: this type is formed as part of this other type

### TODO - Implicit Associations

Associations between types can be made implicitly by enabling the `autorel` modifier:

    use autorel
    
    Carpark {
        contains: Cars[]        // TODO
    }

    Cars {
        color: string
        drivenBy: Driver
    }

    Driver {
        name: string
    }


## TODO

Other things to do:

- Annotation: applicable to diagrams, types, attributes, methods and relationships.  Mainly
  to be used to pass information to modifiers.